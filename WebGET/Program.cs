﻿/*
 * Fetches the Character and Corporation sheets for a 
 * given Pilot from the EveApi and outputs the data
 * to the console.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace WebGET
{
    class Program
    {
        static void Main(string[] args)
        {
            EveApiHandler eveApi    = new EveApiHandler();
            XmlDocument xmlResults  = new XmlDocument();
            StringBuilder results   = new StringBuilder();


            Console.Clear();
            Console.SetWindowSize((int)(Console.LargestWindowWidth * 0.60f), (int)(Console.LargestWindowHeight * 0.75f));

            string characterName = String.Empty;
            while (true)
            {
                // Get name
                // May Arethusa
                Console.Clear();
                Console.Write("Enter character name (! to exit): ");
                characterName = Console.ReadLine();

                
                if (characterName != String.Empty)
                {
                    // Check for debug value
                    if (characterName[0] == '?') characterName = "May Arethusa";
                    // Check for exit character
                    if (characterName[0] == '!') break;

                    // Fetch character information
                    Console.WriteLine("Fetching character info...");
                    EveCharacter may = EveCharacter.LoadCharacter(characterName);
                    if (may.CharacterId != 0)
                    {
                        results.AppendFormat("**** Character ****{0}\n", may);
                    }
                    else
                    {
                        // Character retrival failed, try again
                        Console.WriteLine("No character found");
                        continue;
                    }


                    // Fetch corp info
                    if (may.CorporationId != 0)
                    {
                        Console.WriteLine("Fetching corp info...");
                        EveCorp corp = EveCorp.LoadCorporation(may.CorporationId);
                        results.AppendFormat("\n**** Corporation ****{0}\n", corp);
                    }
                    else
                    {
                        // Corporation retrival failed
                        Console.WriteLine("Corporation not found");
                    }

                    // Output details
                    Console.Clear();
                    Console.WriteLine(results.ToString());
                    results.Clear();

                    // Wait to exit.
                    Console.Write("\nPress any ley...");
                    Console.ReadLine();
                }
            }
            
        }


        static public void OutputAllChildren(XmlNode node)
        {
            Dictionary<string, string> variables = new Dictionary<string, string>();

            // Output all of the nodes children
            XmlNodeList details = node.ChildNodes;

            Console.WriteLine("{0,-20} {1}", node["characterID"].Name, node["characterID"].InnerText);

            foreach (XmlNode n in details)
            {
 
                variables.Add(n["characterID"].Name, n.InnerText);
                Console.WriteLine("{0,-20} {1}", n.Name, n.InnerText);

            }
            
        }
    }
}
