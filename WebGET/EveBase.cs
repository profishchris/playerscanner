﻿/* 
 * Provides a ToString ovveride for our Eve classes to inherit
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace WebGET
{
    abstract public class EveBase
    {     
        public override string ToString()
        {
            // Use string builder for efficency
            StringBuilder builder = new StringBuilder();

            // Loop through each property and stringify name/value pair
            Type classType   = this.GetType();
            string propName  = String.Empty;
            string propValue = String.Empty;
            foreach (var property in classType.GetProperties())
            {
                propName = property.Name;
                propValue = property.GetValue(this, null).ToString();

                // Descriptions contain Html tags which must be removed
                if (propName == "Description")
                {
                    propValue = StripHtml(propValue);
                }

                // Add name/value to the string
                builder.AppendFormat("\n{0,-25}: {1}", propName, propValue);

            }

            // Return finished string
            return builder.ToString();
        }
        

        // This method is rather crude. Would be better to convert html to string
        // TODO replace with HtmlAgility.Convert
        private string StripHtml(string htmlString)
        {
            StringBuilder sb = new StringBuilder(htmlString);

            // Store index of < and it's matching > as a section
            List<Tuple<int, int>> sections = new List<Tuple<int,int>>();
            int startPos = 0;
            for (int i = 0; i < htmlString.Length; i++)
            {
                if (htmlString[i] == '<')
                {
                    startPos = i;
                }
                else if (htmlString[i] == '>')
                {
                    sections.Add(new Tuple<int, int>(startPos, i - startPos + 1));
                }               
            }

            // Starting with the final pair, remove each section
            for (int i = sections.Count - 1; i >= 0; i--)
            {
                sb.Remove(sections[i].Item1, sections[i].Item2);
            }

            WordWrap(sb, Console.WindowWidth - 1);

            // Add starting newline
            sb.Insert(0, '\n');

            return sb.ToString();
        }


        /// <summary>
        /// Wrap a string at a given interval.
        /// </summary>
        /// <param name="sb"> The string to format </param>
        /// <param name="lineLength"> maximum line length </param>
        private void WordWrap(StringBuilder sb, int lineLength = 50)
        {
            // Keep a line count so that we can take add newlines into account
            int lineCount = 0;
            for (int i = lineLength; i < sb.Length; i += lineLength)
            {
                int offset = i + lineCount;
                sb.Insert(offset, '\n');
                lineCount++;
            }

        }
    }
}
