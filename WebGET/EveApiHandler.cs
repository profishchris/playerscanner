﻿/*
 *  All EveAPI calls can be found at http://wiki.eve-id.net/APIv2_Page_Index
 * 
 *  Uses supplied arguments and relative address to return 
 *  XML from the EveAPI.
 *  
 */


using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;

namespace WebGET
{
    class EveApiHandler
    {
        // All EveAPI calls are handled by this address
        private static Uri apiBaseAddresss = new Uri("https://api.eveonline.com");

        private string apiRelativeAddress;
        private NameValueCollection arguments;
        private XmlDocument document;


        // Ctor
        public EveApiHandler()
        {
            apiRelativeAddress  = String.Empty;
            document            = new XmlDocument();
            arguments           = new NameValueCollection();
        }


        // Properties
        public string ApiFunction
        {
            get
            {
                return apiRelativeAddress;
            }
            set
            {
                apiRelativeAddress = value;
            }
        }

        public NameValueCollection Arguments
        {
            get
            {
                return arguments;
            }
            set
            {
                arguments = value;
            }
        }


        #region Methods
        /// <summary>
        /// Sends a GET request to the EveApi.
        /// Uses class data to fill request
        /// </summary>
        /// <returns> XmlDocument containing EveApi XML result </returns>
        public XmlDocument FetchXml()
        {
            string result = String.Empty;

            // Open a WebClient
            using (WebClient client = new WebClient())
            {
                // Load arguments into Query
                client.QueryString = arguments;

                try
                {
                    // Generat XML from query result. Query formated and appended automagically
                    Uri fullApiAddress = new Uri(apiBaseAddresss, apiRelativeAddress);
                    result = client.DownloadString(fullApiAddress);
                }
                catch (WebException e)
                {
                    // Output any errors
                    if (e.Status == WebExceptionStatus.ProtocolError)
                    {
                        Console.Error.WriteLine("{0}: {1}", ((HttpWebResponse)e.Response).StatusCode,
                                                      ((HttpWebResponse)e.Response).StatusDescription);
                        return null;
                    }
                }
            }

            // Error checking
            if (result == String.Empty)
            {
                Console.Error.WriteLine("Error: No response from EveApi");
                return null;
            }

            // Convert result to XML
            document.LoadXml(result);

            return document;
        }

        /// <summary>
        /// Static function.
        /// Deserializes the given XML into the provided type
        /// </summary>
        /// <typeparam name="T"> Type to deserialize into </typeparam>
        /// <param name="node"> XML to deserialize </param>
        /// <returns> Deserialized XML, cast to the correct type </returns>
        public static T Deserialize<T>(XmlNode node)
        {
            // Create a object from the node
            XmlSerializer parser = new XmlSerializer(typeof(T));
            XmlReader reader = XmlReader.Create(new StringReader(node.OuterXml));

            return (T)parser.Deserialize(reader);
        }
        #endregion
    }
}
