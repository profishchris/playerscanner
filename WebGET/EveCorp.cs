﻿/*
 * Models the EveApi Corporation Sheet
 * 
 */


using System;
using System.Xml;
using System.Xml.Serialization;

namespace WebGET
{
    [XmlRoot("result")]
    public class EveCorp : EveBase
    {
        private int     mCorpId;
        private string  mCorpName;
        private string  mCorpTracker;
        private int     mCeoId;
        private int     mStationId;
        private string  mStationName;
        private string  mDescription;
        private string  mWebAddress;
        private int     mAllianceId;
        private string  mAllianceName;
        private double  mTaxRate;
        private int     mMembersCount;
        private long    mShares;


        private EveCorp()
        {
            mCorpId         = 0;
            mCeoId          = 0;
            mStationId      = 0;
            mAllianceId     = 0;
            mMembersCount   = 0;
            mShares         = 0;
            mTaxRate        = 0.0;

            mCorpName       = String.Empty;
            mCorpTracker    = String.Empty;
            mStationName    = String.Empty;
            mDescription    = String.Empty;
            mAllianceName   = String.Empty;
            mWebAddress     = String.Empty;
        }


        public static EveCorp LoadCorporation(int corpId)
        {
            EveApiHandler eveApi = new EveApiHandler();

            // Pull corp info
            eveApi.ApiFunction = "corp/CorporationSheet.xml.aspx";
            eveApi.Arguments.Clear();
            eveApi.Arguments.Add("corporationID", corpId.ToString());
            XmlDocument xmlResults = eveApi.FetchXml();

            // Instantiate EveCorp
            XmlNode corpInfo = xmlResults.SelectSingleNode("/eveapi/result");
            return EveApiHandler.Deserialize<EveCorp>(corpInfo);
        }


        [XmlElementAttribute("corporationID")]
        public int CorporationId
        {
            get { return mCorpId; }
            set { mCorpId = value; }
        }

        [XmlElementAttribute("corporationName")]
        public string CorporationName
        {
            get { return mCorpName; }
            set { mCorpName = value; }
        }

        [XmlElementAttribute("ticker")]
        public string CorporationTracker
        {
            get { return mCorpTracker; }
            set { mCorpTracker = value; }
        }

        [XmlElementAttribute("ceoID")]
        public int CeoId
        {
            get { return mCeoId; }
            set { mCeoId = value; }
        }

        [XmlElementAttribute("stationID")]
        public int StationId
        {
            get { return mStationId; }
            set { mStationId = value; }
        }

        [XmlElementAttribute("stationName")]
        public string StationName
        {
            get { return mStationName; }
            set { mStationName = value; }
        }

        [XmlElementAttribute("description")]
        public string Description
        {
            get { return mDescription; }
            set { mDescription = value; }
        }

        [XmlElementAttribute("url")]
        public string WebAddress
        {
            get { return mWebAddress; }
            set { mWebAddress = value; }
        }
 
        [XmlElementAttribute("allianceID")]
        public int AllianceId
        {
            get { return mAllianceId; }
            set { mAllianceId = value; }
        }
   
        [XmlElementAttribute("allianceName")]
        public string AllianceName
        {
            get { return mAllianceName; }
            set { mAllianceName = value; }
        }
      
        [XmlElementAttribute("taxRate")]
        public double TaxRate
        {
            get { return mTaxRate; }
            set { mTaxRate = value; }
        }

        [XmlElementAttribute("memberCount")]
        public int NoOfMembers
        {
            get { return mMembersCount; }
            set { mMembersCount = value; }
        }

        [XmlElementAttribute("shares")]
        public long Shares
        {
            get { return mShares; }
            set { mShares = value; }
        }
        
        
    }
}
