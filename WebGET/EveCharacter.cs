﻿/*
 * Models the EveApi Character Sheet
 *  
 */

using System;
using System.Xml;
using System.Xml.Serialization;

namespace WebGET
{
    [XmlRoot("result")]
    public class EveCharacter : EveBase
    {
        private string  mName;
        private int     mCharacterId;
        private string  mRace;
        private string  mBloodline;
        private int     mCorpId;
        private string  mCorpName;
        private string  mCorpDate;
        private int     mAllianceId;
        private string  mAllianceName;
        private string  mAllianceDate;
        private double  mSecurityStatus;


        private EveCharacter()
        {
            // Set defaults
            mCharacterId    = 0;
            mCorpId         = 0;
            mAllianceId     = 0;
            mSecurityStatus = 0.0;

            mName           = String.Empty;
            mRace           = String.Empty;
            mBloodline      = String.Empty;
            mCorpName       = String.Empty;
            mCorpDate       = String.Empty;
            mAllianceName   = String.Empty;
            mAllianceDate   = String.Empty;
        }


        /// <summary>
        /// Calls the CharacterInfo function.
        /// Attempts to lookup pilot by name.
        /// </summary>
        /// <param name="name"> CharacterName to lookup </param>
        /// <returns> Blank if not found </returns>
        static public EveCharacter LoadCharacter(string name)
        {
             EveApiHandler eveApi = new EveApiHandler();

            // Query character name
            eveApi.ApiFunction = "eve/CharacterID.xml.aspx";
            eveApi.Arguments.Clear();
            eveApi.Arguments.Add("names", name);
            XmlDocument xmlResults = eveApi.FetchXml();


            if (xmlResults == null) return new EveCharacter();


            // GET charcterID.
            XmlNode node = xmlResults.SelectSingleNode("/eveapi/result/rowset/row");
            int id = 0;

            // If node is empty then create a blank character            
            Int32.TryParse(node.Attributes["characterID"].Value, out id);


            // Character is correct, populate
            return LoadCharacter(id);
        }


        /// <summary>
        /// Calls the CharacterInfo function.
        /// Assumes correct id.
        /// </summary>
        /// <param name="id"> CharacterId to lookup  </param>
        /// <returns> Populated EveCharacter </returns>
        static public EveCharacter LoadCharacter(int id)
        {
            EveApiHandler eveApi = new EveApiHandler();

            // Pull all charcter info
            eveApi.ApiFunction = "eve/CharacterInfo.xml.aspx";
            eveApi.Arguments.Clear();
            eveApi.Arguments.Add("characterID", id.ToString());
            XmlDocument xmlResults = eveApi.FetchXml();


            if (xmlResults == null) return new EveCharacter();


            // Parse character info
            XmlNode charInfo = xmlResults.SelectSingleNode("/eveapi/result");
            

            return EveApiHandler.Deserialize<EveCharacter>(charInfo);
        }


        #region Properties
        [XmlElementAttribute("characterName")]
        public string Name
        {
            get { return mName; }
            set { mName = value; }
        }


        [XmlElementAttribute("characterID")]
        public int CharacterId
        {
            get { return mCharacterId; }
            set { mCharacterId = value; }
        }


        [XmlElementAttribute("race")]
        public string Race
        {
            get { return mRace; }
            set { mRace = value; }
        }


        [XmlElementAttribute("bloodline")]
        public string Bloodline
        {
            get { return mBloodline; }
            set { mBloodline = value; }
        }


        [XmlElementAttribute("corporationID")]
        public int CorporationId
        {
            get { return mCorpId; }
            set { mCorpId = value; }
        }


        [XmlElementAttribute("corporation")]
        public string CorporationName
        {
            get { return mCorpName; }
            set { mCorpName = value; }
        }


        [XmlElementAttribute("corporationDate")]
        public string CorporationStartDate
        {
            get { return mCorpDate; }
            set { mCorpDate = value; }
        }


        [XmlElementAttribute("allianceID")]
        public int AllianceId
        {
            get { return mAllianceId; }
            set { mAllianceId = value; }
        }


        [XmlElementAttribute("alliance")]
        public string AllianceName
        {
            get { return mAllianceName; }
            set { mAllianceName = value; }
        }


        [XmlElementAttribute("allianceDate")]
        public string AllianceStartDate
        {
            get { return mAllianceDate; }
            set { mAllianceDate = value; }
        }


        [XmlElementAttribute("securityStatus")]
        public double SecurityStatus
        {
            get { return mSecurityStatus; }
            set { mSecurityStatus = value; }
        }
        #endregion

    }
}
